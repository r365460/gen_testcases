import sys
import numpy as np
filename = sys.argv[1]

perm = []

with open(filename, "r") as f:
    perm.append( [ int(x) for x in f.readline().split() ] )
    perm.append( [ int(x) for x in f.readline().split() ] )
    perm.append( [ int(x) for x in f.readline().split() ] )

# Read Input
    shape = [ int(x) for x in f.readline().split() ]
    # print('Input shape: ', shape)

    datain = list()
    for i in range(shape[2]):
        buffer = list()
        for j in range(shape[1]):
            data = [ int(x) for x in f.readline().split() ]
            buffer.append(data)
        datain.append(buffer)

# Read Output
    f.readline()
    shape = [ int(x) for x in f.readline().split()]
    # print('Output shape: ', shape)

    dataout = list()
    for i in range(shape[2]):
        buffer = list()
        for j in range(shape[1]):
            data = [ int(x) for x in f.readline().split() ]
            buffer.append(data)
        dataout.append(buffer)

# Check Slice
    datain = np.array(datain)
    dataout = np.array(dataout)

    print('Perms: ', )
    print(perm)
    starts = [ perm[0][0], perm[1][0], perm[2][0] ]
    ends =   [ perm[0][1], perm[1][1], perm[2][1] ]
    steps =  [ perm[0][2], perm[1][2], perm[2][2] ]

    print('DataIn shape: ', datain.shape)
    ans = datain[ starts[2]:ends[2]:steps[2],
                     starts[1]:ends[1]:steps[1],
                     starts[0]:ends[0]:steps[0]
            ];

    print('After slice DataIn shape: ', ans.shape)
    print('DataOut shape: ', dataout.shape)
    if np.array_equal(ans, dataout) or (ans.size==0 and dataout.size==0):
        print(" ======= YES, two cubes are equal =======")
    else:
        print(ans)
        print("Output")
        print(dataout)
        print("NONONONONONONONONONONONONONONONONONO")
        print("NONONONONONONONONONONONONONONONONONO")
        print("NONONONONONONONONONONONONONONONONONO")
        print("NONONONONONONONONONONONONONONONONONO")
