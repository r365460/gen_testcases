#include "Register.hpp"

void Register::fill_register_with_in_cube(const Cube& cube) {

	W = cube.W;
	H = cube.H;
	C = cube.C;
	line_stride[DATAIN] = cube.line_stride > 0? cube.line_stride:0;
	surf_stride[DATAIN] = cube.surf_stride > 0? cube.surf_stride:0;
	planar_stride[DATAIN] = cube.planar_stride > 0? cube.planar_stride:0;

	high_address[DATAIN] = 0;
	// 27 bits
	low_address[DATAIN] = (start_address>>5) + cube.address_entry;
}

void Register::fill_register_with_out_cube(const Cube& cube) {

	line_stride[DATAOUT] = cube.line_stride > 0? cube.line_stride:0;
	surf_stride[DATAOUT] = cube.surf_stride > 0? cube.surf_stride:0;
	planar_stride[DATAOUT] = cube.planar_stride > 0? cube.planar_stride:0;

	high_address[DATAOUT] = 0;
	// 27 bits
	low_address[DATAOUT] = (start_address>>5) + cube.address_entry;

	dataout_channel = cube.C;

}

void Register::write_json_file(string file_json, string mem_in_file, string mem_out_file, 
		const vector<Register>& regs, const Cube& in_cube, const Cube& out_cube) 
{
	FILE* fp = fopen(file_json.c_str(), "w");
	fprintf(fp, "{\n\t\"comments\": [\n\t\t \"Update 07/22: make comments a list\" \n\t],\n");
	fprintf(fp, "\t\"input_data_file\": \"%s\", \n", mem_in_file.c_str());
	fprintf(fp, "\t\"expected_output_data\": \"%s\", \n", mem_out_file.c_str());

	long long input_load_to = (start_address>>5) + in_cube.address_entry;
	long long output_load_to = (start_address>>5) + out_cube.address_entry;
	long long output_length = out_cube.used_entry_number();
	fprintf(fp, "\t\"overall_input_addr\": %lld, \n", input_load_to);
	fprintf(fp, "\t\"overall_output_addr\": %lld, \n", output_load_to);
	fprintf(fp, "\t\"overall_output_length\": %lld, \n\n", output_length);

	fprintf(fp, "\t\"register_for_each_operation\" : { \n");
	for(int i=0; i<regs.size(); i++) {
		fprintf(fp, "\t\t\"%d\" : {\n", i);
			fprintf(fp, "\t\t\t\"rubik_mode\" : %d,\n", regs[i].rubik_mode); 
			fprintf(fp, "\t\t\t\"in_precision\" : %d,\n", regs[i].in_precison); 
			fprintf(fp, "\t\t\t\"deconv_x_stride\" : %d,\n", regs[i].deconv_x_stride);
			fprintf(fp, "\t\t\t\"deconv_y_stride\" : %d,\n\n", regs[i].deconv_y_stride);

			fprintf(fp, "\t\t\t\"datain_width\" : %d,\n", regs[i].W);
			fprintf(fp, "\t\t\t\"datain_height\" : %d,\n", regs[i].H);
			fprintf(fp, "\t\t\t\"datain_channel\" : %d,\n", regs[i].C);
			fprintf(fp, "\t\t\t\"datain_ram_type\" : %d,\n", regs[i].ram_type[DATAIN]);
			fprintf(fp, "\t\t\t\"dain_addr_high\" : %d,\n", regs[i].high_address[DATAIN]);
			fprintf(fp, "\t\t\t\"dain_addr_low\" : %d,\n", regs[i].low_address[DATAIN]);
			fprintf(fp, "\t\t\t\"dain_line_stride\" : %d,\n", regs[i].line_stride[DATAIN]);
			fprintf(fp, "\t\t\t\"dain_surf_stride\" : %d,\n", regs[i].surf_stride[DATAIN]);
			fprintf(fp, "\t\t\t\"dain_planar_stride\" : %d,\n\n", regs[i].planar_stride[DATAIN]);

			fprintf(fp, "\t\t\t\"dataout_channel\" : %d,\n", regs[i].dataout_channel);
			fprintf(fp, "\t\t\t\"dataout_ram_type\" : %d,\n", regs[i].ram_type[DATAOUT]);
			fprintf(fp, "\t\t\t\"daout_addr_high\" : %d,\n", regs[i].high_address[DATAOUT]);
			fprintf(fp, "\t\t\t\"daout_addr_low\" : %d,\n", regs[i].low_address[DATAOUT]);
			fprintf(fp, "\t\t\t\"daout_line_stride\" : %d,\n", regs[i].line_stride[DATAOUT]);
			fprintf(fp, "\t\t\t\"daout_surf_stride\" : %d,\n", regs[i].surf_stride[DATAOUT]);
			fprintf(fp, "\t\t\t\"daout_planar_stride\" : %d,\n\n", regs[i].planar_stride[DATAOUT]);

			fprintf(fp, "\t\t\t\"contract_stride_0\" : %d,\n", regs[i].contract_stride_0);
			fprintf(fp, "\t\t\t\"contract_stride_1\" : %d\n", regs[i].contract_stride_1);

		fprintf(fp, "\t\t}");
		if(i != regs.size() -1) fprintf(fp, ",");
		fprintf(fp, "\n");
	}
	fprintf(fp, "\t},\n");
	fprintf(fp, "\t\"hw_config\" : \"nv_full\"\n");

	fprintf(fp, "}");
	fclose(fp);
}

void Register::write_json_file(string file_json, string mem_in_file, string mem_out_file,
		const Register& reg, const Cube& in_cube, const Cube& out_cube){
	write_json_file(file_json, mem_in_file, mem_out_file, vector<Register>{reg}, in_cube, out_cube);
}

void Register::write_cfg_file(string file, string mem_in_file, string mem_out_file, 
		const vector<Register>& regs, const Memory& mem, const Cube& in_cube, const Cube& out_cube) {

	FILE* fp = fopen(file.c_str(), "w");

	fprintf(fp, "mem_init(pri_mem, 0x%08llx, 0x%x, ALL_ZERO);\n", start_address, (mem.total_entry()<<5) );
	fprintf(fp, "mem_load(pri_mem, 0x%08llx, \"%s\");\n\n\n",start_address, mem_in_file.c_str());
	for(auto const &reg: regs) {

		fprintf(fp, "reg_write(NVDLA_RUBIK.S_POINTER_0, 0x0);\n");

		int precision = elementSize == 1? 0 : 1;
		int d_misc_cfg = (precision << 8) | reg.rubik_mode;
		fprintf(fp, "reg_write(NVDLA_RUBIK.D_MISC_CFG_0, 0x%08x);\n", d_misc_cfg);

		int data_in_size0 = ( (reg.H-1) << 13) | (reg.W-1);
		int data_in_size1 = (reg.C-1);

		fprintf(fp, "reg_write(NVDLA_RUBIK.D_DAIN_RAM_TYPE_0      ,0x%08x);\n", reg.ram_type[DATAIN]);
		fprintf(fp, "reg_write(NVDLA_RUBIK.D_DATAIN_SIZE_0_0      ,0x%08x);\n", data_in_size0);
		fprintf(fp, "reg_write(NVDLA_RUBIK.D_DATAIN_SIZE_1_0      ,0x%08x);\n", data_in_size1);
		fprintf(fp, "reg_write(NVDLA_RUBIK.D_DAIN_ADDR_HIGH_0     ,0x%08x);\n", reg.high_address[DATAIN]);
		fprintf(fp, "reg_write(NVDLA_RUBIK.D_DAIN_ADDR_LOW_0      ,0x%08x);\n", reg.low_address[DATAIN]<<5);
		fprintf(fp, "reg_write(NVDLA_RUBIK.D_DAIN_LINE_STRIDE_0   ,0x%08x);\n", reg.line_stride[DATAIN]<<5);
		fprintf(fp, "reg_write(NVDLA_RUBIK.D_DAIN_SURF_STRIDE_0   ,0x%08x);\n", reg.surf_stride[DATAIN]<<5);
		fprintf(fp, "reg_write(NVDLA_RUBIK.D_DAIN_PLANAR_STRIDE_0 ,0x%08x);\n", reg.planar_stride[DATAIN]<<5);

		int data_out_size1 = (reg.dataout_channel-1);
		fprintf(fp, "reg_write(NVDLA_RUBIK.D_DAOUT_RAM_TYPE_0     ,0x%08x);\n", reg.ram_type[DATAOUT]);
		fprintf(fp, "reg_write(NVDLA_RUBIK.D_DATAOUT_SIZE_1_0     ,0x%08x);\n", data_out_size1);
		fprintf(fp, "reg_write(NVDLA_RUBIK.D_DAOUT_ADDR_HIGH_0    ,0x%08x);\n", reg.high_address[DATAOUT]);
		fprintf(fp, "reg_write(NVDLA_RUBIK.D_DAOUT_ADDR_LOW_0     ,0x%08x);\n", reg.low_address[DATAOUT]<<5); 
		fprintf(fp, "reg_write(NVDLA_RUBIK.D_DAOUT_LINE_STRIDE_0  ,0x%08x);\n", reg.line_stride[DATAOUT]<<5);
		fprintf(fp, "reg_write(NVDLA_RUBIK.D_DAOUT_SURF_STRIDE_0  ,0x%08x);\n", reg.surf_stride[DATAOUT]<<5);
		fprintf(fp, "reg_write(NVDLA_RUBIK.D_DAOUT_PLANAR_STRIDE_0,0x%08x);\n", reg.planar_stride[DATAOUT]<<5);

		fprintf(fp, "reg_write(NVDLA_RUBIK.D_CONTRACT_STRIDE_0_0  ,0x%08x);\n", reg.contract_stride_0<<5);
		fprintf(fp, "reg_write(NVDLA_RUBIK.D_CONTRACT_STRIDE_1_0  ,0x%08x);\n", reg.contract_stride_1<<5);
		int deconv_stride = ((reg.deconv_y_stride-1) << 16) | (reg.deconv_x_stride-1);	
		fprintf(fp, "reg_write(NVDLA_RUBIK.D_DECONV_STRIDE_0      ,0x%08x);\n", deconv_stride);

		fprintf(fp, "reg_write(NVDLA_RUBIK.D_OP_ENABLE_0, 0x1);\n");
		fprintf(fp, "intr_notify(RUBIK_0, layer0_RBK_0_INTERRUPT);\n");

		fprintf(fp, "\n\n\n");
	}

	fprintf(fp, "check_file(layer0_RBK_0_INTERRUPT, pri_mem, 0x%08llx, 0x%x, \"%s\");\n", 
			start_address + (out_cube.address_entry<<5), 
			(out_cube.used_entry_number()<<5),
			mem_out_file.c_str());

	fclose(fp);
}

void Register::write_cfg_file(string file, string mem_in_file, string mem_out_file, const Register& reg,
		const Memory& mem, const Cube& in_cube, const Cube& out_cube) {
	write_cfg_file(file, mem_in_file, mem_out_file, {reg}, mem, in_cube, out_cube);
}
