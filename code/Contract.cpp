#include "Contract.hpp"

vector<int> Contract::get_new_shape(int W, int H, int C, int deconv_stride[2]) {

	assert(C %(deconv_stride[0] * deconv_stride[1]) == 0);
	return vector<int>{W * deconv_stride[0], H * deconv_stride[1], C / (deconv_stride[0]*deconv_stride[1])};
}


int Contract::get_contract_stride_0(int C, int surf_stride[2], int deconv_stride[2])
{	
	assert(surf_stride[0] > 0);
	assert(surf_stride[1] > 0);
	assert(deconv_stride[0] >0);
	assert(deconv_stride[1] > 0);

	int contract_stride_0 = surf_stride[0] * C/(ATOMIC_K*deconv_stride[0]*deconv_stride[1]) ;

	return contract_stride_0 ;
}

int Contract::get_contract_stride_1(int line_stride[2], int deconv_stride[2])
{	
	assert(line_stride[0] > 0);
	assert(line_stride[1] > 0);
	assert(deconv_stride[0] >0);
	assert(deconv_stride[1] > 0);

	int contract_stride_1 = deconv_stride[1] * line_stride[1];

	return contract_stride_1 ;
}


void Contract::init_stride(int W, int H, int C, int line_stride[2], int surf_stride[2], int deconv_stride[2], int contract_stride[2]) {

	vector<int> new_shape = get_new_shape(W, H, C, deconv_stride);
	if(line_stride[DATAIN] == -1 or surf_stride[DATAIN] == -1) {
		line_stride[DATAIN] = Cube::get_compact_line_stride(Cube::TYPE::CUBE, W, H, C);
		surf_stride[DATAIN] = Cube::get_compact_surface_stride(Cube::TYPE::CUBE, W, H, C);
	}
	//planar_stride[DATAIN] = -1;

	if(line_stride[DATAOUT] == -1 or surf_stride[DATAOUT] == -1) {
		line_stride[DATAOUT] = Cube::get_compact_line_stride(Cube::TYPE::CUBE, new_shape);
		surf_stride[DATAOUT] = Cube::get_compact_surface_stride(Cube::TYPE::CUBE, new_shape);
	}
	//planar_stride[DATAOUT] = -1;
	
	if(deconv_stride[0]==-1 or deconv_stride[1]==-1)
	{
		deconv_stride[0] = 1;
		deconv_stride[1] = 1;
	}
	
	if(contract_stride[0] == -1)
	{	
		contract_stride[0] = get_contract_stride_0(C, surf_stride, deconv_stride);
	}

	if(contract_stride[1] == -1)
	{	
		contract_stride[1] = get_contract_stride_1(line_stride, deconv_stride);
	}
}

void Contract::contract(Cube& in_cube, Cube& out_cube, Memory& mem, Register& reg, int deconv_stride[2], int contract_stride[2]) {

	reg.set_mode(CONTRACTMODE);
	reg.deconv_x_stride = deconv_stride[0];
	reg.deconv_y_stride = deconv_stride[1];
	reg.contract_stride_0 = contract_stride[0];
	reg.contract_stride_1 = contract_stride[1];
	reg.fill_register_with_in_cube(in_cube);
	reg.fill_register_with_out_cube(out_cube);

	in_cube.load_from_memory(mem);

	vector<int> new_shape = get_new_shape(in_cube.W, in_cube.H, in_cube.C, deconv_stride);
	assert(new_shape[0] == out_cube.W);
	assert(new_shape[1] == out_cube.H);
	assert(new_shape[2] == out_cube.C);

	for(int i=0; i<out_cube.C; i++) {
		for(int j=0; j<out_cube.H; j++) {
			for(int k=0; k<out_cube.W; k++) {

				int wi = (k/deconv_stride[0]);
				int surf_x = (k%deconv_stride[0]);
				int hi = (j/deconv_stride[1]);
				int surf_y = (j%deconv_stride[1]);
				
				int ci = i + out_cube.C * ((surf_y * deconv_stride[0]) + surf_x );

//				printf("wi=%d\tsurf_x=%d\thi=%d\tsurf_y=%d\tci=%d", wi, surf_x, hi, surf_y, ci);
				
				assert(ci>=0 && ci < in_cube.C);
				assert(hi>=0 && hi < in_cube.H);
				assert(wi>=0 && wi < in_cube.W);
				
				out_cube.data[i][j][k] = in_cube.data[ci][hi][wi];
//				printf("\tout_cube.data[%d][%d][%d]= %04x", i, j, k, out_cube.data[i][j][k]);
//				printf("\tin_cube.data[%d][%d][%d]=%04x\n", ci, hi, wi, in_cube.data[ci][hi][wi]);
			}
		}
	}

	out_cube.write_to_memory(mem);
}
