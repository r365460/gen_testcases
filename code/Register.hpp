#ifndef REGISTER_H
#define REGISTER_H

#include "header.hpp"
#include "Cube.hpp"
#include "Memory.h"

class Register {

public:
	void set_mode(int mode) { rubik_mode = mode; }
	void fill_register_with_in_cube(const Cube& cube);
	void fill_register_with_out_cube(const Cube& cube);

	Register() { 
		if(elementSize == 1) in_precison = 0;
		else if(elementSize == 2) in_precison = 1;
		else in_precison = -1;
	}

	int ram_type[2] = {MCIF, MCIF};
	int in_precison = -1;
	int rubik_mode;
	int W, H, C;

	int line_stride[2];
	int surf_stride[2];
	int planar_stride[2];
	int high_address[2];
	int low_address[2];

	int dataout_channel = 0;
	int deconv_x_stride = 0;
	int deconv_y_stride = 0;

	int contract_stride_0 = 0;
	int contract_stride_1 = 0;

	static void write_json_file(string file, string mem_in_file, string mem_out_file, 
								const vector<Register>& regs, const Cube& in_cube, const Cube& out_cube);
	static void write_json_file(string file, string mem_in_file, string mem_out_file, 
								const Register& reg, const Cube& in_cube, const Cube& out_cube);

	static void write_cfg_file(string file, string mem_in_file, string mem_out_file, 
			const vector<Register>& regs, const Memory& mem, const Cube& in_cube, const Cube& out_cube);
	static void write_cfg_file(string file, string mem_in_file, string mem_out_file, const Register& reg, 
			const Memory& mem, const Cube& in_cube, const Cube& out_cube);

	void set_address(int mode, int entry);
};
#endif
