#include "Cube.hpp"

Cube::Cube(TYPE type, int w, int h, int c, int line_stride, int surf_stride, int planar_stride) {

	W = w;
	H = h;
	C = c;

	this->line_stride = line_stride;
	this->surf_stride = surf_stride;
	this->planar_stride = planar_stride;
	this->type = type;

	address_entry = -1;
	//create_buffer();
}

void Cube::allocate_memory(Memory& mem) {
	address_entry = mem.request_memory( used_entry_number() );
}

int Cube::used_entry_number() const {
	if(type == TYPE::CUBE) return surf_stride * ceil(1.0 * C / ATOMIC_K);
	else return C * planar_stride;
}

void Cube::create_buffer() {

	data.resize(C);
	for(int i=0; i<C; i++) {
		data[i].resize(H);
		for(int j=0; j<H; j++) data[i][j].resize(W);
	}
}

void Cube::regular_gen() {

	int id = 0;
	create_buffer();
	for(int i=0; i<C; i++) {
		for(int j=0; j<H; j++) {
			for(int k=0; k<W; k++) {
				data[i][j][k] = id++;
				id %= (1<<(elementSize*8));
			}
		}
	}
}

void Cube::write_to_memory(Memory& mem) {

	if(address_entry == -1) 
		cout << "[Error] Cube has not allocated memory before called write_to_memory\n";
	
	if(type == TYPE::CUBE) write_cube_to_memory(mem);
	else write_planar_to_memory(mem);
}

void Cube::write_cube_to_memory(Memory& mem) {

	for(int surf_id=0; surf_id<ceil(1.0*C/ATOMIC_K); surf_id++) {
		for(int line_id = 0; line_id < H; line_id++) {
			for(int column_id = 0; column_id < W; column_id++) {
				int entry_id = address_entry + surf_id*surf_stride + line_id*line_stride + column_id;

				for(int i=0; i<ATOMIC_K; i++) {
					if(surf_id * ATOMIC_K + i >= C) break;
					int x = data[surf_id*ATOMIC_K + i][line_id][column_id];

					write_element_to_memory(mem, entry_id, i, x);
				}
			}
		}
	}
}

void Cube::write_planar_to_memory(Memory& mem) {

	for(int planar_id=0; planar_id < C; planar_id++) {
		for(int line_id = 0; line_id < H; line_id++) {
			for(int i=0; i<ceil(1.0*W/ATOMIC_K); i++) {
				int entry_id = address_entry + planar_stride*planar_id + line_stride*line_id + i;

				for(int j=0; j<ATOMIC_K; j++) {
					if(i*ATOMIC_K + j >= W) break;
					int x = data[planar_id][line_id][i*ATOMIC_K + j];
					write_element_to_memory(mem, entry_id, j, x);
				}
			}
		}
	}
}

void Cube::write_element_to_memory(Memory&mem, int entry_id, int id, int data) {

	if(elementSize == 1) mem.write(entry_id, id, data);
	else {
		// 左邊是小的
		mem.write(entry_id, id*2, data&(0xff));
		mem.write(entry_id, id*2+1,((data&(0xff00))>>8));
	}
}

void Cube::load_from_memory(Memory& mem) {

	create_buffer();
	if(address_entry == -1) 
		cout << "[Error] Cube has not allocated memory before called load_from_memory\n";

	if(type == TYPE::CUBE) load_cube_from_memory(mem);
	else load_planar_from_memory(mem);
}

void Cube::load_cube_from_memory(Memory& mem) {

	for(int surf_id=0; surf_id<ceil(1.0 * C / ATOMIC_K); surf_id++) {
		for(int line_id = 0; line_id < H; line_id++) {
			for(int column_id=0; column_id<W; column_id++) {
				int entry = address_entry + surf_id * surf_stride + line_id * line_stride + column_id;

				for(int c=0; c<ATOMIC_K; c++) {
					int c_id = surf_id * ATOMIC_K + c;
					if(c_id >= C) break;

					data[c_id][line_id][column_id] = load_element_from_memory(mem, entry, c);
				}
			}
		}
	}
}

void Cube::load_planar_from_memory(Memory& mem) {

	for(int planar_id=0; planar_id < C; planar_id++) {
		for(int line_id=0; line_id < H; line_id++) {
			for(int i=0; i<ceil(1.0 * W / ATOMIC_K); i++) {
				int entry_id = address_entry + planar_id * planar_stride + line_id * line_stride + i;

				for(int j=0; j<ATOMIC_K; j++) {
					if(i*ATOMIC_K + j >= W) break;

					data[planar_id][line_id][i*ATOMIC_K + j] = load_element_from_memory(mem, entry_id, j);
				}
			}
		}
	}
}

int Cube::load_element_from_memory(Memory& mem, int entry_id, int id) {

	if(elementSize==2) return mem.read(entry_id, id*2) | (mem.read(entry_id, id*2+1)<<8);
	else return mem.read(entry_id, id);
}
	
bool Cube::operator == (const Cube& rh) const {

	if(W != rh.W ) return false;
	if(H != rh.H ) return false;
	if(C != rh.C ) return false;

// call load_from_memory first
	assert(rh.C == rh.data.size());
	assert(rh.H == rh.data[0].size());
	assert(rh.W == rh.data[0][0].size());

	assert(C == data.size());
	assert(H == data[0].size());
	assert(W == data[0][0].size());

	for(int i=0; i<C; i++) {
		for(int j=0; j<H; j++) {
			for(int k=0; k<W; k++){ 
				if(data[i][j][k] != rh.data[i][j][k]) return false;
			}
		}
	}
	return true;
}

int Cube::get_compact_line_stride(TYPE type, const vector<int>& shape) {
	assert(shape.size() == 3);
	return get_compact_line_stride(type, shape[0], shape[1], shape[2]);
}

int Cube::get_compact_planar_stride(TYPE type, const vector<int>& shape) {
	assert(shape.size() == 3);
	return get_compact_planar_stride(type, shape[0], shape[1], shape[2]);
}

int Cube::get_compact_surface_stride(TYPE type, const vector<int>& shape) {
	assert(shape.size() == 3);
	return get_compact_surface_stride(type, shape[0], shape[1], shape[2]);
}

int Cube::get_compact_line_stride(TYPE type, int W, int H, int C) {
	if(type == TYPE::CUBE) return W%2==0? W:W+1;
	else {
		int num = ceil(1.0 * W / ATOMIC_K);
		return num%2==0? num:(num + 1);
	}
}

int Cube::get_compact_planar_stride(TYPE type, int W, int H, int C) {
	if(type == TYPE::CUBE) cout << "[Warning] Type::Cube don't have planar stride\n";
	else return get_compact_line_stride(type, W, H, C) * H;

	return 0;
}

int Cube::get_compact_surface_stride(TYPE type, int W, int H, int C) {
	if(type == TYPE::PLANAR) cout << "[Warning] Type::Planar don't have surface stride\n";
	else return get_compact_line_stride(type, W, H, C) * H;

	return 0;
}

void Cube::print() {
	cout << get_type() << " shape = " << W << " x " << H << " x " << C << endl;
	for(const auto& planar: data) {
		for(const auto& line : planar) {
			for(const auto& x: line) {
				cout << x << " ";
			}
			cout << endl;
		}
		cout << endl;
	}
}

// call load_from_memory first
void Cube::dump_to_file(string filename, string mode) {
	FILE* fp = fopen(filename.c_str(), mode.c_str());

	fprintf(fp, "%d %d %d\n", W, H, C);
	assert(data.size() == C);
	if(C != 0) assert(data[0].size() == H);
	if(C != 0 and H != 0) assert(data[0][0].size() == W);

	cout << "DUMP TO FILE: " << W << " " << H << " " << C << endl;
	for(int i=0; i<C; i++) {
		for(int j=0; j<H; j++) {
			for(int k=0; k<W; k++) {
				fprintf(fp, "%d ", data[i][j][k]);
			}
			fprintf(fp, "\n");
		}
	}
	fprintf(fp, "\n");
	fclose(fp);
}
