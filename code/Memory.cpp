#include "Memory.hpp"

Memory::Memory(int memWidth) : memWidth(memWidth)
{
	used_entry = 0;
}

Memory::BYTE Memory::read(int entry, int id) { // return one byte;
	if(entry >= mem.size() or id >= memWidth) {
		cout << "[Error] error address (" << entry << ", " << id << ") to read memory\n";
		exit(0);
		return 0;
	}
	else return mem[entry][id];
}

void Memory::write(int entry, int id, BYTE data) {
	if(data > 255) { 
		cout << "[Warning] write data: " << data << " to (" 
			<< entry << ", " << id << ") is bigger than one byte\n";
	}

	if(id>=memWidth) {
		cout << "[Error] write data: " << data << " to (" << entry << ", " << id << ")" << endl;
		exit(0);
	}

	if(entry>=mem.size()) {
		cout << "[Error] Write to unrequested entry " << entry << " " << mem.size() << "\n";
		exit(0);
	}

	mem[entry][id] = data;
}

void Memory::dump_to_file(string fileName) {
	FILE* fp = fopen(fileName.c_str(), "w");
	int offset = 0;
	for(const auto& row: mem) {
		fprintf(fp, "{offset:0x%x, size:%d, payload:", offset, memWidth);
		for(int i=0; i<row.size(); i++) {
			if(i != 0) fprintf(fp, " ");
			fprintf(fp, "0x%02x", row[i]);
		}
		fprintf(fp, "},\n");

		offset += memWidth;
	}
	fclose(fp);
}

int Memory::request_memory(int total_entry) {
	int given_entry = used_entry;
	used_entry += total_entry;
	for(int i=0; i<total_entry; i++) {
		mem.emplace_back(vector<int>(memWidth, 0));
	}
	assert(mem.size() == used_entry);
	return given_entry;
}

int Memory::total_entry() const {
	return mem.size();
}

