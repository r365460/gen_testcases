#ifndef CONTRACT_H
#define CONTRACT_H

#include "header.hpp"
#include "Cube.hpp"
#include "Memory.hpp"
#include "Register.hpp"

class Contract {

public:
	static void init_stride(int W, int H, int C, int line_stride[2], int surf_stride[2], int deconv_stride[2], int contract_stride[2]);
	static vector<int> get_new_shape(int W, int H, int C, int deconv_stride[2]);

	static void contract(Cube& in_cube, Cube& out_cube, Memory& mem, Register& reg, int deconv_stride[2], int contract_stride[2]);
	
	static int get_contract_stride_0(int C, int surf_stride[2], int deconv_stride[2]);
	static int get_contract_stride_1(int line_stride[2], int deconv_stride[2]);
};
#endif
