#ifndef SPLIT_H
#define SPLIT_H

#include "header.hpp"
#include "Cube.hpp"
#include "Register.hpp"

using namespace std;

class Split {
public:
	static void init_stride(int W, int H, int C, int line_stride[2], int surf_stride[2], int planar_stride[2]);

	static void split(Cube& in_cube, Cube& out_cube, Memory& mem, Register& reg);
};

#endif
