#ifndef OPERATOR_H
#define OPERATOR_H

#include "header.hpp"
#include "Memory.hpp"
#include "Register.hpp"
#include "Cube.hpp"
#include "Split.hpp"
#include "Merge.hpp"
#include "Contract.hpp"

class TransposeParm {

public:
	
	TransposeParm(){}

	TransposeParm(std::initializer_list<int> li) {
		for(const auto& i : li)
			parm.push_back(i);
	}

	string type() const {
		assert(parm.size() == 3);
		if(parm[0]==1 and parm[1]==0 and parm[2]==2) return "HWC";
		else if(parm[0]==1 and parm[1]==2 and parm[2]==0) return "HCW";
		else if(parm[0]==2 and parm[1]==0 and parm[2]==1) return "CWH";
		else if(parm[0]==2 and parm[1]==1 and parm[2]==0) return "CHW";
		else if(parm[0]==0 and parm[1]==2 and parm[2]==1) return "WCH";
		else return "WHC";
	}

	int size() const { return parm.size(); }

	vector<int> parm;
};

class Transpose {

public:
	static TransposeParm read_attribute(int argc, char* argv[], int index);

	static void init_stride(int W, int H, int C, int line_stride[2], int surf_stride[2], 
			int planar_stride[2], const TransposeParm& parm);

	static void transpose(Cube& in_cube, Cube& out_cube, Memory& mem, 
			vector<Register>& reg, const TransposeParm& parm);

	static vector<int> get_new_shape(int W, int H, int C, const TransposeParm& parm);
	static vector<int> get_new_shape(const vector<int>& shape, const TransposeParm& parm);

private:
	static void transpose_W_C(Cube& in_cube, Cube& out_cube, Memory& mem, vector<Register>& reg);
	static void transpose_W_H(Cube& in_cube, Cube& out_cube, Memory& mem, vector<Register>& reg);
};

class SliceParm {

public:
	SliceParm() {}

	SliceParm(std::initializer_list<vector<int>> li) {

		for(const auto& i : li)
			parm.push_back(i);
	}

	int size() const { return parm.size(); }

	vector<vector<int>> parm;
};

class Slice {

public:
	static SliceParm read_attribute(int argc, char* [], int index);

	static void init_stride(int W, int H, int C, int line_stride[2], int surf_stride[2], 
			int planar_stride[2], const SliceParm& parm);

	static vector<int> get_new_shape(int W, int H, int C, const SliceParm& parm);
	static vector<int> get_new_shape(const vector<int>& shape, const SliceParm& parm);

	static void slice(Cube& input_cube, Cube& output_cube, Memory& mem, 
			vector<Register>& regs, const SliceParm& parm);

	static void print_information(int W, int H, int C, const SliceParm& parm);

private:
	static void slice_w(Cube& input_cube, Cube& output_cube, Memory& mem, 
			vector<Register>& regs, const vector<int>& parm);
	static void slice_h(Cube& input_cube, Cube& output_cube, Memory& mem, 
			vector<Register>& regs, const vector<int>& parm);
	static void slice_c(Cube& input_cube, Cube& output_cube, Memory& mem, 
			vector<Register>& regs, const vector<int>& parm);

	static void slice_c_positive_step(Cube& input_cube, Cube& output_cube, Memory& mem, 
			vector<Register>& regs, const vector<int>& parm);
	static void slice_c_negative_step(Cube& input_cube, Cube& output_cube, Memory& mem, 
			vector<Register>& regs, const vector<int>& parm);

	static vector<int> get_valid_start_end_step(const vector<int>& parm, int lim);
	static bool is_stop(int i, int end, int step) {
		if(step < 0) return i >= end;
		else return i <= end;
	};
};

#endif

