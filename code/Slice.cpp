#include "Operators.hpp"

SliceParm Slice::read_attribute(int argc, char* argv[], int index) {
	SliceParm parm;
	parm.parm.resize(3);
	while( index < argc) {
		string opt(argv[index++]);
		int which;
		if(opt=="-axisW") which = 0;
		else if(opt=="-axisH") which = 1;
		else if(opt=="-axisC") which = 2;
		else {
			cout << "Slice error attribute: " << opt << endl;
			exit(-1);
		}

		parm.parm[which].push_back( atoi(argv[index++]) ); // start
		parm.parm[which].push_back( atoi(argv[index++]) ); // end
		parm.parm[which].push_back( atoi(argv[index++]) ); // step
	}
	return parm;
}

void Slice::init_stride(int W, int H, int C, int line_stride[2], int surf_stride[2],
		int planar_stride[2], const SliceParm& parm)
{
	
	if(line_stride[DATAIN]==-1 or surf_stride[DATAIN] == -1) {
		line_stride[DATAIN] = Cube::get_compact_line_stride(Cube::TYPE::CUBE, W, H, C);
		surf_stride[DATAIN] = Cube::get_compact_surface_stride(Cube::TYPE::CUBE, W, H, C);
	}
	planar_stride[DATAIN] = -1;

	vector<int> new_shape = get_new_shape(W, H, C, parm);
	if(line_stride[DATAOUT]==-1 or surf_stride[DATAOUT] == -1) {
		line_stride[DATAOUT] = Cube::get_compact_line_stride(Cube::TYPE::CUBE, new_shape);
		surf_stride[DATAOUT] = Cube::get_compact_surface_stride(Cube::TYPE::CUBE, new_shape);
	}
	planar_stride[DATAOUT] = -1;
}

// get the valid segment [a, b]
vector<int> Slice::get_valid_start_end_step(const vector<int>& parm, int lim) {

	assert(parm.size() == 3);
	int start = parm[0] >= 0? parm[0]:parm[0]+lim;
	int end   = parm[1] >= 0? parm[1]:parm[1]+lim;
	int step  = parm[2];

	if(step == 0) {
		cout << "[Error] slice: step cannot be zero\n";
		exit(-1);
	}
	if(step > 0) {
		if(start < 0) start = 0;
		//if(start >= lim) {} => cause zero dim

		if(end >= lim) end = lim-1;
		else end -= 1;
	}
	else {
		if(start >= lim) start = lim-1;
		//if(start < 0) { } => cause zero dim
		
		if(end < 0) end = 0;
		else end++;
	}
	return {start, end, step};
}

vector<int> Slice::get_new_shape(const vector<int>& shape, const SliceParm& parm) {
	assert(shape.size() == 3);
	return get_new_shape(shape[0], shape[1], shape[2], parm);
}

vector<int> Slice::get_new_shape(int W, int H, int C, const SliceParm& parm) {

	auto cal = [](const vector<int> &parm, int lim) -> int{
		vector<int> new_parm = get_valid_start_end_step(parm, lim);
		int start = new_parm[0];
		int end = new_parm[1];
		int step = new_parm[2];

		assert(step!=0);
		if(step < 0) { // 4 0 -1
			if(start == lim) start--;

			if(start < end) return 0; // 0 4 -1
			else return 1 + (start - end)/std::abs(step);
		}
		else {
			if(start > end) return 0;
			else return 1 + (end - start)/step;
		}
	};

	vector<int> shape = {W, H, C};
	vector<int> new_shape = {W, H, C};
	string name = "WHC";

	assert(parm.size() == 3);
	for(int i=0; i<3; i++) if(parm.parm[i].size()) {
		assert(parm.parm[i].size() == 3);
		new_shape[i] = cal(parm.parm[i], shape[i]);
		if(new_shape[i] <= 0) cout << "[Warning] slice get zero " << name[i] << endl;
	}

	return new_shape;
}

void Slice::print_information(int W, int H, int C, const SliceParm& parm) {
	vector<int> new_shape = get_new_shape(W, H, C, parm);

	cout << "Slice:\n";
	cout << "Input Shape: " << W << " x " << H << " x " << C << endl;
	cout << "Output Shape: " << new_shape[0] << " x " << new_shape[1] << " x " << new_shape[2] << endl;
	assert(parm.size() == 3);
	for(int i=0; i<3; i++) if(parm.parm[i].size()){
		assert(parm.parm[i].size() == 3);
		cout << i << " => ";
		cout << parm.parm[i][0] << " : " << parm.parm[i][1] << " : " << 
			parm.parm[i][2] << " = " << new_shape[i] << endl;
	}
}

void Slice::slice(Cube& input_cube, Cube& output_cube, Memory& mem, vector<Register>& regs, 
		const SliceParm& parm) 
{
	assert(parm.size() == 3);
	input_cube.load_from_memory(mem);

	vector<Cube> buffer;
	buffer.push_back(input_cube);

	// input_cube -> slice W -> slice H -> slice C -> output_cube
	if(parm.parm[0].size()) {
		vector<int> new_shape = get_new_shape(buffer.back().W, buffer.back().H, buffer.back().C,
				{parm.parm[0], vector<int>(), vector<int>()}) ;

		Cube slice_w_output = Cube(Cube::TYPE::CUBE, new_shape[0], new_shape[1], new_shape[2],
				Cube::get_compact_line_stride(Cube::TYPE::CUBE, new_shape),
				Cube::get_compact_surface_stride(Cube::TYPE::CUBE, new_shape),
				-1
		);
		slice_w_output.allocate_memory(mem);
		// 
		slice_w(buffer.back(), slice_w_output, mem, regs, parm.parm[0]);

		buffer.push_back(slice_w_output);
	}
	if(parm.parm[1].size()) {
		vector<int> new_shape = get_new_shape(buffer.back().W, buffer.back().H, buffer.back().C,
				{{}, parm.parm[1], {}}) ;

		Cube slice_h_output = Cube(Cube::TYPE::CUBE, new_shape[0], new_shape[1], new_shape[2],
				Cube::get_compact_line_stride(Cube::TYPE::CUBE, new_shape),
				Cube::get_compact_surface_stride(Cube::TYPE::CUBE, new_shape),
				-1
		);
		slice_h_output.allocate_memory(mem);
		slice_h(buffer.back(), slice_h_output, mem, regs, parm.parm[1]);

		buffer.push_back(slice_h_output);
	}
	if(parm.parm[2].size()) {
		vector<int> new_shape = get_new_shape(buffer.back().W, buffer.back().H, buffer.back().C,
				{{}, {}, parm.parm[2]}) ;

		Cube slice_c_output = Cube(Cube::TYPE::CUBE, new_shape[0], new_shape[1], new_shape[2],
				Cube::get_compact_line_stride(Cube::TYPE::CUBE, new_shape),
				Cube::get_compact_surface_stride(Cube::TYPE::CUBE, new_shape),
				-1
		);
		slice_c_output.allocate_memory(mem);
		slice_c(buffer.back(), slice_c_output, mem, regs, parm.parm[2]);

		buffer.push_back(slice_c_output);
	}

	output_cube = buffer.back();
}

void Slice::slice_w(Cube& input_cube, Cube& output_cube, Memory& mem, vector<Register>& regs, 
		const vector<int>& parm) 
{
	vector<int> new_perm = get_valid_start_end_step(parm, input_cube.W);
	int start = new_perm[0];
	int end = new_perm[1];
	int step = new_perm[2];

	if(output_cube.W != 0) {
		assert(0<=start and start < input_cube.W);
		assert(0<=end and end < input_cube.W);
	}

	// use split and merge to copy
	// input_cube (1 * H * C) =>(split) split_output=>(merge) => output_cube
	//
	Cube split_output(Cube::TYPE::PLANAR, 1, input_cube.H, input_cube.C,
		Cube::get_compact_line_stride(Cube::TYPE::PLANAR, 1, input_cube.H, input_cube.C),
		-1,
		Cube::get_compact_planar_stride(Cube::TYPE::PLANAR, 1, input_cube.H, input_cube.C)
	);
	split_output.allocate_memory(mem);

	for(int i=start, output_put_id = 0; is_stop(i, end, step); i+=step, output_put_id++) {
		Cube split_input(Cube::TYPE::CUBE, 1, input_cube.H, input_cube.C,
			input_cube.line_stride,
			input_cube.surf_stride,
			input_cube.planar_stride
		);
		split_input.address_entry = input_cube.address_entry + i;

		Register reg_split;
		Split::split(split_input, split_output, mem, reg_split);
		regs.push_back(reg_split);

		Cube merge_output(Cube::TYPE::CUBE, 1, input_cube.H, input_cube.C,
				output_cube.line_stride,
				output_cube.surf_stride,
				output_cube.planar_stride
		);
		merge_output.address_entry = output_cube.address_entry + output_put_id;
		Register reg_merge;
		Merge::merge(split_output, merge_output, mem, reg_merge);
		regs.push_back(reg_merge);
	}
	output_cube.load_from_memory(mem);
}

void Slice::slice_h(Cube& input_cube, Cube& output_cube, Memory& mem, vector<Register>& regs, 
		const vector<int>& parm) 
{
	vector<int> new_perm = get_valid_start_end_step(parm, input_cube.H);
	int start = new_perm[0];
	int end = new_perm[1];
	int step = new_perm[2];

	vector<int> new_shape = get_new_shape(input_cube.W, input_cube.H, 
			input_cube.C, {{}, parm, {}});

	assert(output_cube.W == new_shape[0]);
	assert(output_cube.H == new_shape[1]);
	assert(output_cube.C == new_shape[2]);
	Cube split_total_out(Cube::TYPE::PLANAR, new_shape[0], new_shape[1], new_shape[2],
			Cube::get_compact_line_stride(Cube::TYPE::PLANAR, new_shape),
			-1,
			Cube::get_compact_planar_stride(Cube::TYPE::PLANAR, new_shape)
	);
	split_total_out.allocate_memory(mem);

	for(int i=start, output_put_id = 0; is_stop(i, end, step); i+=step, output_put_id++) {
		Cube split_input(Cube::TYPE::CUBE, input_cube.W, 1, input_cube.C,
			input_cube.line_stride,
			input_cube.surf_stride,
			input_cube.planar_stride
		);
		split_input.address_entry = input_cube.address_entry + i * input_cube.line_stride;

		Cube split_output(Cube::TYPE::PLANAR, input_cube.W, 1, input_cube.C,
			split_total_out.line_stride,
			split_total_out.surf_stride,
			split_total_out.planar_stride
		);
		split_output.address_entry = split_total_out.address_entry +
							 output_put_id * split_total_out.line_stride;

		Register reg;
		Split::split(split_input, split_output, mem, reg);
		regs.push_back(reg);
	}

	Register reg;
	Merge::merge(split_total_out, output_cube, mem, reg);
	regs.push_back(reg);
}

void Slice::slice_c(Cube& input_cube, Cube& output_cube, Memory& mem, vector<Register>& regs, 
		const vector<int>& parm) 
{
	Slice::slice_c_negative_step(input_cube, output_cube, mem, regs, parm);
	//int step = parm[2];
	//if(step > 0) Slice::slice_c_positive_step(input_cube, output_cube, mem, regs, perm);
	//else Slice::slice_c_negative_step(input_cube, output_cube, mem, regs, perm);
}

void Slice::slice_c_positive_step(Cube& input_cube, Cube& output_cube, Memory& mem, vector<Register>& regs, const vector<int>& parm) 
{
	vector<int> new_perm = get_valid_start_end_step(parm, input_cube.C);
	int start = new_perm[0];
	int end = new_perm[1];
	int step = new_perm[2];

	vector<int> new_shape = get_new_shape(input_cube.W, input_cube.H, 
			input_cube.C, {{}, {}, parm});

	assert(output_cube.W == new_shape[0]);
	assert(output_cube.H == new_shape[1]);
	assert(output_cube.C == new_shape[2]);
	Cube split_out(Cube::TYPE::PLANAR, input_cube.W, input_cube.H, input_cube.C,
			Cube::get_compact_line_stride(Cube::TYPE::PLANAR, input_cube.W, input_cube.H, input_cube.C),
			-1,
			Cube::get_compact_planar_stride(Cube::TYPE::PLANAR, input_cube.W, input_cube.H, input_cube.C)
	);
	split_out.allocate_memory(mem);

	Register reg;
	Split::split(input_cube, split_out, mem, reg);
	regs.push_back(reg);

	split_out.address_entry = split_out.address_entry + start * split_out.planar_stride;
	split_out.planar_stride *= step;
	split_out.C = new_shape[2];

	Register reg_merge;
	Merge::merge(split_out, output_cube, mem, reg_merge);
	regs.push_back(reg_merge);
}

void Slice::slice_c_negative_step(Cube& input_cube, Cube& output_cube, Memory& mem, vector<Register>& regs, const vector<int>& parm) 
{
// 1. transpose to W
	vector<int> new_shape = Transpose::get_new_shape(input_cube.W, 
			input_cube.H, input_cube.C, {2, 1, 0});
	Cube after_transpose(Cube::TYPE::CUBE, new_shape[0], new_shape[1], new_shape[2],
			Cube::get_compact_line_stride(Cube::TYPE::CUBE, new_shape),
			Cube::get_compact_surface_stride(Cube::TYPE::CUBE, new_shape),
			-1
	);
	after_transpose.allocate_memory(mem);

	Transpose::transpose(input_cube, after_transpose, mem, regs, {2, 1, 0});

// 2. slice
	vector<int> new_shape_slice = Slice::get_new_shape(after_transpose.W, 
			after_transpose.H, after_transpose.C, {parm, {}, {}});
	Cube after_slice(Cube::TYPE::CUBE, new_shape_slice[0], new_shape_slice[1], new_shape_slice[2],
			Cube::get_compact_line_stride(Cube::TYPE::CUBE, new_shape_slice),
			Cube::get_compact_surface_stride(Cube::TYPE::CUBE, new_shape_slice),
			-1
	);
	after_slice.allocate_memory(mem);
	slice_w(after_transpose, after_slice, mem, regs, parm);

// 3. transpose back
	vector<int> new_shape_tra = Transpose::get_new_shape(after_slice.W, 
			after_slice.H, after_slice.C, {2, 1, 0});

	assert(output_cube.W == new_shape_tra[0]);
	assert(output_cube.H == new_shape_tra[1]);
	assert(output_cube.C == new_shape_tra[2]);
	Transpose::transpose(after_slice, output_cube, mem, regs, {2, 1, 0});
}
