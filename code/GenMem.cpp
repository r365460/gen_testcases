#include "header.hpp"
#include "Memory.hpp"
#include "Register.hpp"
#include "Cube.hpp"
#include "Split.hpp"
#include "Merge.hpp"
#include "Contract.hpp"
#include "Operators.hpp"

unsigned long long start_address = 0xC0000000;
int elementSize;
int ATOMIC_K;

const int memWidth = 32;

void new_version(int argc, char* argv[]) {
	if(argc <= 6) {
		cout << "./GenMem W H C elementSize(1 or 2)\
			filename \
			cmd [attri]" ;
		exit(0);
	}
	int W = atoi(argv[1]), H = atoi(argv[2]), C = atoi(argv[3]);
	elementSize = atoi(argv[4]);
	ATOMIC_K = (elementSize == 1? 32:16);
	string filename(argv[5]);
	string cmd(argv[6]);

	cout << "memory width = " << memWidth << " bytes\n";
	cout << "element size = " << elementSize << " bytes\n";
	cout << "ATOMIC_K = " << ATOMIC_K << endl;
	cout << "Command = " << cmd << endl;

	cout << "Data Information:\n";
	cout << "\tW = " << W << "\n";
	cout << "\tH = " << H << "\n";
	cout << "\tC = " << C << "\n";

	string file_datain = filename + ".in";
	string file_dataout = filename + ".out";
	string file_cfg = filename + ".cfg";
	string file_json = filename + ".js";
	cout << "Output File:\n";
	cout << "\t" << file_datain.c_str() << endl;
	cout << "\t" << file_dataout.c_str() << endl;
	cout << "\t" << file_cfg.c_str() << endl;
	cout << "\t" << file_json.c_str() << endl;

	int line_stride[2] = {-1, -1};
	int surf_stride[2] = {-1, -1};
	int planar_stride[2] = {-1, -1};

	int i = 7;
	while(i < argc) {
		string attr = argv[i++];
		if(i >= argc) {
			cout << argv[i] << " Error \n";
			exit(0);
		}
		if(attr=="-perm")  break;
		else {
			if(attr=="-datain_line_stride")         line_stride[DATAIN] = atoi(argv[i++]);
			else if(attr=="-datain_surf_stride")    surf_stride[DATAIN] = atoi(argv[i++]);
			else if(attr=="-datain_planar_stride")  planar_stride[DATAIN] = atoi(argv[i++]);
			else if(attr=="-dataout_line_stride")   line_stride[DATAOUT] = atoi(argv[i++]);
			else if(attr=="-dataout_surf_stride")   surf_stride[DATAOUT] = atoi(argv[i++]);
			else if(attr=="-dataout_planar_stride") planar_stride[DATAOUT] = atoi(argv[i++]);
			else cout << argv[i] << " Error \n";
		}
	}

	Memory mem(memWidth);

	if(cmd == "split") {
	// 0. decide tht input, output stride
		Split::init_stride(W, H, C, line_stride, surf_stride, planar_stride);

	// 1. generate a input cube
		Cube in_cube(Cube::TYPE::CUBE, W, H, C,
				line_stride[DATAIN], surf_stride[DATAIN], planar_stride[DATAIN]
		);
		in_cube.allocate_memory(mem);
		in_cube.regular_gen();
		in_cube.write_to_memory(mem);
		mem.dump_to_file(file_datain);

	// 2. create a output cube
		Cube out_cube(Cube::TYPE::PLANAR, W, H, C,
				line_stride[DATAOUT], surf_stride[DATAOUT], planar_stride[DATAOUT]
		);
		out_cube.allocate_memory(mem);

	// 3. split
		Register reg;
		Split::split(in_cube, out_cube, mem, reg);

    // 4. dump register
		Register::write_json_file(file_json, file_datain, file_dataout, reg, in_cube, out_cube);
		Register::write_cfg_file(file_cfg, file_datain, file_dataout, reg, mem, in_cube, out_cube);

	// 5. write output cube to file
		Memory mem_out(memWidth);
		out_cube.load_from_memory(mem);
		out_cube.address_entry = 0;
		assert( mem_out.request_memory( out_cube.used_entry_number() ) == 0);
		out_cube.write_to_memory(mem_out);
		mem_out.dump_to_file(file_dataout);

		assert( mem_out.total_entry() == out_cube.used_entry_number() );
	}

	else if(cmd == "merge") {
	// 0. decide tht input, output stride
		Merge::init_stride(W, H, C, line_stride, surf_stride, planar_stride);

	// 1. generate a input cube (planar)
		Cube in_cube(Cube::TYPE::PLANAR, W, H, C,
				line_stride[DATAIN], surf_stride[DATAIN], planar_stride[DATAIN]
		);
		in_cube.allocate_memory(mem);
		in_cube.regular_gen();
		in_cube.write_to_memory(mem);
		mem.dump_to_file(file_datain);

	// 2. create a output cube
		Cube out_cube(Cube::TYPE::CUBE, W, H, C,
				line_stride[DATAOUT], surf_stride[DATAOUT], planar_stride[DATAOUT]
		);
		out_cube.allocate_memory(mem);

	// 3. merge
		Register reg;
		Merge::merge(in_cube, out_cube, mem, reg);

    // 4. dump register
		Register::write_json_file(file_json, file_datain, file_dataout, reg, in_cube, out_cube);
		Register::write_cfg_file(file_cfg, file_datain, file_dataout, reg, mem, in_cube, out_cube);

	// 5. write output cube to file
		Memory mem_out(memWidth);
		out_cube.load_from_memory(mem);
		out_cube.address_entry = 0;
		assert( mem_out.request_memory( out_cube.used_entry_number() ) == 0);
		out_cube.write_to_memory(mem_out);
		mem_out.dump_to_file(file_dataout);
	}

	else if(cmd == "transpose") {
	// 0. decide tht input, output stride
		auto transpose_parm = Transpose::read_attribute(argc, argv, i);

		Transpose::init_stride(W, H, C, line_stride, surf_stride, planar_stride, transpose_parm);

	// 1. generate a input cube 
		Cube in_cube(Cube::TYPE::CUBE, W, H, C,
				line_stride[DATAIN], surf_stride[DATAIN], planar_stride[DATAIN]
		);
		in_cube.allocate_memory(mem);
		in_cube.regular_gen();
		in_cube.write_to_memory(mem);
		mem.dump_to_file(file_datain);

	// 2. create a output cube
		vector<int> new_shape = Transpose::get_new_shape(W, H, C, transpose_parm);
		Cube out_cube(Cube::TYPE::CUBE, new_shape[0], new_shape[1], new_shape[2], 
				line_stride[DATAOUT], surf_stride[DATAOUT], planar_stride[DATAOUT]
		);
		out_cube.allocate_memory(mem);


	// 3. transpose 
		vector<Register> regs;
		Transpose::transpose(in_cube, out_cube, mem, regs, transpose_parm);
		Register::write_cfg_file(file_cfg, file_datain, file_dataout, regs, mem, in_cube, out_cube);

    // 4. dump register
		Register::write_json_file(file_json, file_datain, file_dataout, regs, in_cube, out_cube);

	// 5. write output cube to file
		Memory mem_out(memWidth);
		out_cube.load_from_memory(mem);
		out_cube.address_entry = 0;     // change the offset
		assert( mem_out.request_memory( out_cube.used_entry_number() ) == 0);
		out_cube.write_to_memory(mem_out); // write to output memory file
		mem_out.dump_to_file(file_dataout);

	// check output
		in_cube.load_from_memory(mem);
		out_cube.load_from_memory(mem_out);

		string transpose_out = "transpose_out.txt";
		FILE* fp = fopen(transpose_out.c_str(), "w");
		fprintf(fp, "%d %d %d\n", transpose_parm.parm[0], transpose_parm.parm[1], 
				transpose_parm.parm[2]);
		fclose(fp);
		in_cube.dump_to_file(transpose_out, "a");
		out_cube.dump_to_file(transpose_out, "a");
		//system("python check_transpose.py transpose_out.txt");
	}

	else if(cmd=="slice") {
	// 0. decide tht input, output stride
		auto slice_parm = Slice::read_attribute(argc, argv, i);
		Slice::init_stride(W, H, C, line_stride, surf_stride, planar_stride, slice_parm);


	// 1. generate a input cube 
		Cube in_cube(Cube::TYPE::CUBE, W, H, C,
				line_stride[DATAIN], surf_stride[DATAIN], planar_stride[DATAIN]
		);
		in_cube.allocate_memory(mem);
		in_cube.regular_gen();
		in_cube.write_to_memory(mem);
		mem.dump_to_file(file_datain);

	// 2. create a output cube
		vector<int> new_shape = Slice::get_new_shape(W, H, C, slice_parm);
		Cube out_cube(Cube::TYPE::CUBE, new_shape[0], new_shape[1], new_shape[2], 
				line_stride[DATAOUT], surf_stride[DATAOUT], planar_stride[DATAOUT]);
		out_cube.allocate_memory(mem);

		Slice::print_information(W, H, C, slice_parm);

	// 3. slice
		vector<Register> regs;

		Slice::slice(in_cube, out_cube, mem, regs, slice_parm);

    // 4. dump register
		Register::write_json_file(file_json, file_datain, file_dataout, regs, in_cube, out_cube);
		Register::write_cfg_file(file_cfg, file_datain, file_dataout, regs, mem, in_cube, out_cube);

	// 5. write output cube to file
		Memory mem_out(memWidth);
		out_cube.load_from_memory(mem);
		out_cube.address_entry = 0;
		assert( mem_out.request_memory( out_cube.used_entry_number() ) == 0);
		out_cube.write_to_memory(mem_out);
		mem_out.dump_to_file(file_dataout);

	// check output
		in_cube.load_from_memory(mem);
		out_cube.load_from_memory(mem_out);

		string slice_out = "slice_out.txt";
		FILE* fp = fopen(slice_out.c_str(), "w");

		assert(slice_parm.size() == 3);
		int lim = max({in_cube.W, in_cube.H, in_cube.C});
		for(int i=0; i<slice_parm.size(); i++) {
			if(slice_parm.parm[i].size() == 0) fprintf(fp, "%d %d %d\n", 0, lim, 1);
			else fprintf(fp, "%d %d %d\n", slice_parm.parm[i][0], 
					slice_parm.parm[i][1], slice_parm.parm[i][2]);
		}
		fclose(fp);
		in_cube.dump_to_file(slice_out, "a");
		out_cube.dump_to_file(slice_out, "a");

		//cout << "Input: \n"; in_cube.print();
		//cout << "Output: \n"; out_cube.print();
	}
	//cout << "Memory used entry: " << mem.total_entry() << endl;

}

int main(int argc, char* argv[]) 
{
	new_version(argc, argv);
	return 0;
}
