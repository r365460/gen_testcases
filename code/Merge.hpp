#ifndef MERGE_H
#define MERGE_H

#include "header.hpp"
#include "Cube.hpp"
#include "Register.hpp"

class Merge {

public:
	static void init_stride(int W, int H, int C, int line_stride[2], int surf_stride[2], int planar_stride[2]);
	static void merge(Cube& in_cube, Cube& out_cube, Memory& mem, Register& reg);
};
#endif
