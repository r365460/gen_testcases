#ifndef MEMORY_H
#define MEMORY_H

#include "header.hpp"

class Memory {

	using BYTE = int;
public:
	Memory(int memWidth);

	BYTE read(int entry, int id);
	void write(int entry, int id, BYTE data);

	void dump_to_file(string fileName);

	int request_memory(int total_entry);

	int total_entry() const;

private:
	vector<vector<BYTE>> mem;
	int memWidth;
	int used_entry;
};

#endif
