#ifndef HEADER_H
#define HEADER_H

#include <iostream>
#include <vector>
#include <cmath>
#include <cassert>
#include <algorithm>

using std::cin;
using std::cout;
using std::endl;

using std::ceil;

using std::vector;
using std::string;

using std::min;

#define DATAIN 0
#define DATAOUT 1

#define CVIF 0
#define MCIF 1

#define CONTRACTMODE 0
#define SPLITMODE    1
#define MERGEMODE    2

#define MAX_MEMORY_ENTRY 33554430

// define in GenMem.cpp
extern unsigned long long start_address; 
extern int ATOMIC_K; 
extern int elementSize;

#endif
