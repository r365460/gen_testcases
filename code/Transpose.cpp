#include "Operators.hpp"

TransposeParm Transpose::read_attribute(int argc, char* argv[], int index) {

	TransposeParm parm;
	parm.parm.push_back( atoi(argv[index++]) );
	parm.parm.push_back( atoi(argv[index++]) );
	parm.parm.push_back( atoi(argv[index++]) );

	return parm;
}

void Transpose::init_stride(int W, int H, int C, int line_stride[2], int surf_stride[2], int planar_stride[2], const TransposeParm& parm) {

	assert(parm.size() == 3);
	vector<int> new_shape = get_new_shape(W, H, C, parm); 

	if(line_stride[DATAIN] == -1 or surf_stride[DATAIN] == -1) {
		line_stride[DATAIN] = Cube::get_compact_line_stride(Cube::TYPE::CUBE, W, H ,C);
		surf_stride[DATAIN] = Cube::get_compact_surface_stride(Cube::TYPE::CUBE, W, H ,C);
	}
	planar_stride[DATAIN] = -1;

	if(line_stride[DATAOUT] == -1 or surf_stride[DATAOUT] == -1) {
		line_stride[DATAOUT] = Cube::get_compact_line_stride(Cube::TYPE::CUBE, new_shape);
		surf_stride[DATAOUT] = Cube::get_compact_surface_stride(Cube::TYPE::CUBE, new_shape);
	}
	planar_stride[DATAOUT] = -1;
}

vector<int> Transpose::get_new_shape(const vector<int>& shape, const TransposeParm& perm) {
	assert(shape.size() == 3);
	return get_new_shape(shape[0], shape[1], shape[2], perm);
}

vector<int> Transpose::get_new_shape(int W, int H, int C, const TransposeParm& parm) {

	assert(parm.size() == 3);
	vector<int> old_shape = {W, H, C};

	return vector<int>{old_shape[parm.parm[0]], old_shape[parm.parm[1]], old_shape[parm.parm[2]]};
}

void Transpose::transpose(Cube& in_cube, Cube& out_cube, Memory& mem, 
		vector<Register>& regs, const TransposeParm& parm) 
{

	in_cube.load_from_memory(mem);

	// C H W
	if(parm.type() == "CHW") transpose_W_C(in_cube, out_cube, mem, regs);

	// H W C
	else if(parm.type() == "HWC") transpose_W_H(in_cube, out_cube, mem, regs);

	// H C W
	else if(parm.type() == "HCW") {

		// step 1. get C H W
		vector<int> new_shape = get_new_shape(in_cube.W, in_cube.H, in_cube.C, {2, 1, 0});
		Cube chw_cube(Cube::TYPE::CUBE, new_shape[0], new_shape[1], new_shape[2], 
				Cube::get_compact_line_stride(Cube::TYPE::CUBE, new_shape),
				Cube::get_compact_surface_stride(Cube::TYPE::CUBE, new_shape),
				-1
		);
		chw_cube.allocate_memory(mem);

		transpose_W_C(in_cube, chw_cube, mem, regs);

		// step 2. get H C W
		transpose_W_H(chw_cube, out_cube, mem, regs);
	}

	// C W H
	else if(parm.type() == "CWH") {

		// step 1. get H W C
		vector<int> new_shape = get_new_shape(in_cube.W, in_cube.H, in_cube.C, {1, 0, 2});
		Cube hwc_cube(Cube::TYPE::CUBE, new_shape[0], new_shape[1], new_shape[2], 
				Cube::get_compact_line_stride(Cube::TYPE::CUBE, new_shape),
				Cube::get_compact_surface_stride(Cube::TYPE::CUBE, new_shape),
				-1
		);
		hwc_cube.allocate_memory(mem);

		transpose_W_H(in_cube, hwc_cube, mem, regs);

		// step 2. get C W H
		transpose_W_C(hwc_cube, out_cube, mem, regs);
	}

	// W C H
	else if(parm.type() == "WCH") {
		
		// step 1. get C W H
		vector<int> new_shape = get_new_shape(in_cube.W, in_cube.H, in_cube.C, {2, 0, 1});
		Cube cwh_cube(Cube::TYPE::CUBE, new_shape[0], new_shape[1], new_shape[2], 
				Cube::get_compact_line_stride(Cube::TYPE::CUBE, new_shape),
				Cube::get_compact_surface_stride(Cube::TYPE::CUBE, new_shape),
				-1
		);
		cwh_cube.allocate_memory(mem);

		transpose(in_cube, cwh_cube, mem, regs, {2, 0, 1});

		// step. get w c h
		transpose_W_H(cwh_cube, out_cube, mem, regs);
	}
}

void Transpose::transpose_W_C(Cube& input_cube, Cube& output_cube, Memory& mem, 
		vector<Register>& regs) 
{
	Cube before_slice_cube(output_cube.type, output_cube.W*2, output_cube.H, output_cube.C, 
			output_cube.line_stride * 2,
			output_cube.surf_stride * 2,
			output_cube.planar_stride
	);
	before_slice_cube.allocate_memory(mem);

	for(int i=0; i<ceil(1.0 * input_cube.W/ATOMIC_K); i++) {
		int w = min(ATOMIC_K, input_cube.W - i * ATOMIC_K);

		// Get ATOMIC_K * H * C by changing address_entry
		Cube in_cube_split(Cube::TYPE::CUBE, w, input_cube.H, input_cube.C,
				input_cube.line_stride, input_cube.surf_stride, input_cube.planar_stride
		);
		in_cube_split.address_entry = input_cube.address_entry + i * ATOMIC_K;

		Cube out_cube_split(Cube::TYPE::PLANAR, w, input_cube.H, input_cube.C,
				before_slice_cube.line_stride, -1, 2
		);
		out_cube_split.address_entry = before_slice_cube.address_entry + i * before_slice_cube.surf_stride;

		Register reg;
		Split::split(in_cube_split, out_cube_split, mem, reg);
		regs.push_back(reg);
	}

	Slice::slice(before_slice_cube, output_cube, mem, regs, {{0, before_slice_cube.W, 2}, {}, {}});
}

void Transpose::transpose_W_H(Cube& input_cube, Cube& output_cube, Memory& mem, 
		vector<Register>& regs) 
{
	Cube before_slice_cube(output_cube.type, output_cube.W*2, output_cube.H, output_cube.C, 
			output_cube.line_stride * 2,
			output_cube.surf_stride * 2,
			output_cube.planar_stride
	);
	before_slice_cube.allocate_memory(mem);

	// create a split cube for reusing every time
	int W = 1, H = input_cube.H, C = input_cube.C;
	int line_stride = Cube::get_compact_line_stride(Cube::TYPE::PLANAR, W, H, C);
	int planar_stride = Cube::get_compact_planar_stride(Cube::TYPE::PLANAR, W, H, C);
	Cube out_cube_split(Cube::TYPE::PLANAR, W, H, C, line_stride, -1, planar_stride);
	out_cube_split.allocate_memory(mem);

	for(int i=0; i<input_cube.W; i++) {

		// Get 1* H * C by changing address_entry
		Cube in_cube_split(Cube::TYPE::CUBE, W, H, C,
				input_cube.line_stride, input_cube.surf_stride, input_cube.planar_stride
		);
		in_cube_split.address_entry = input_cube.address_entry + i;

		Register reg_split;
		Split::split(in_cube_split, out_cube_split, mem, reg_split);
		regs.push_back(reg_split);

		// adjusting the line_stride and surface_stride 
		Cube out_cube_merge(Cube::TYPE::CUBE, W, H, C,
				2, before_slice_cube.surf_stride, -1
		);
		out_cube_merge.address_entry = before_slice_cube.address_entry + i * before_slice_cube.line_stride;

		Register reg_merge;
		Merge::merge(out_cube_split, out_cube_merge, mem, reg_merge);
		regs.push_back(reg_merge);
	}

	//output_cube=before_slice_cube;
	Slice::slice(before_slice_cube, output_cube, mem, regs, {{0, before_slice_cube.W, 2}, {}, {}});
}
