#ifndef CUBE_H
#define CUBE_H

#include "header.hpp"
#include "Memory.hpp"

using namespace std;

class Cube {

public:
	enum class TYPE {
		CUBE,
		PLANAR
	};

	Cube(TYPE type, int w, int h, int c, int line, int surf, int planar);
	Cube() {}

	string get_type(){ return type==TYPE::CUBE? "CUBE":"PLANAR"; }
	void allocate_memory(Memory& mem);
	void regular_gen();
	void load_from_memory(Memory& mem);
	void write_to_memory(Memory& mem);

	void create_buffer();
	int used_entry_number() const;

	void print();
	void dump_to_file(string filename, string mode="w");

	bool operator == (const Cube& rh) const;

	static int get_compact_line_stride(TYPE type, int W, int H, int C);
	static int get_compact_planar_stride(TYPE type, int W, int H, int C);
	static int get_compact_surface_stride(TYPE type, int W, int H, int C);

	static int get_compact_line_stride(TYPE type,    const vector<int>& shape);
	static int get_compact_planar_stride(TYPE type,  const vector<int>& shape);
	static int get_compact_surface_stride(TYPE type, const vector<int>& shape);

	TYPE type;
	int W, H, C;
	int line_stride, surf_stride, planar_stride;
	int address_entry;
	vector<vector<vector<int>>> data;

private:
	void load_cube_from_memory(Memory& mem);
	void load_planar_from_memory(Memory& mem);
	int load_element_from_memory(Memory& mem, int entry_id, int id);

	void write_cube_to_memory(Memory& mem);
	void write_planar_to_memory(Memory& mem);
	void write_element_to_memory(Memory&mem, int entry_id, int id, int data);
};

#endif
