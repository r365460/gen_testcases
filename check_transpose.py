import sys
import numpy as np
filename = sys.argv[1]

with open(filename, "r") as f:
    perm = [ int(x) for x in f.readline().split() ]
    print('Input perm: ', perm)

# Read Input
    shape = [ int(x) for x in f.readline().split()]
    # print('Input shape: ', shape)

    datain = list()
    for i in range(shape[2]):
        buffer = list()
        for j in range(shape[1]):
            data = [ int(x) for x in f.readline().split() ]
            buffer.append(data)
        datain.append(buffer)

# Read Output
    f.readline()
    shape = [ int(x) for x in f.readline().split()]
    # print('Output shape: ', shape)

    dataout = list()
    for i in range(shape[2]):
        buffer = list()
        for j in range(shape[1]):
            data = [ int(x) for x in f.readline().split() ]
            buffer.append(data)
        dataout.append(buffer)

# Check Transpose
    datain = np.array(datain)
    dataout = np.array(dataout)

    perm[0], perm[2] = perm[2], perm[0]
    mapping = {0:2, 1:1, 2:0};
    perm = [ mapping[x] for x in perm];
    # print('Adjust perm: ', perm)
    print('DataIn shape: ', datain.shape)
    datain = np.transpose(datain, perm)
    print('After Transpose DataIn shape: ', datain.shape)
    print('DataOut shape: ', dataout.shape)
    if np.array_equal(datain, dataout):
        print("=========== YES, two cubes are equal ===========")
    else:
        print("NONONONONONONONONONONONONONONONONONO")
        print("NONONONONONONONONONONONONONONONONONO")
        print("NONONONONONONONONONONONONONONONONONO")
        print("NONONONONONONONONONONONONONONONONONO")
        print("NONONONONONONONONONONONONONONONONONO")
