elementSize=2

W=23
H=101
C=113

cd code/
make
cp GenMem* ../
cd ../
folder=./testcases/slice/slice_both_h_w/small_w/
cmd=slice
prefix=slice_both_h_w_small_w


filename=$prefix'_'big_c_posStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisW 0 23 2 -axisH 17 101 11 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

filename=$prefix'_'big_c_negStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisW 23 0 -2 -axisH 101 17 -11 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt


W=23
H=101
C=13

filename=$prefix'_'small_c_posStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisW 1 23 2 -axisH 0 101 11 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

filename=$prefix'_'small_c_negStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisW 45 -23 -2 -axisH 301 -123 11 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

mv $prefix* $folder
