elementSize=2

W=163
H=101
C=113

cd code/
make
cp GenMem* ../
cd ../
folder=./testcases/slice/slice_both_c_w/big_w/
cmd=slice
prefix=slice_both_c_w_big_w

filename=$prefix'_'big_c_posStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisW 64 128 2 -axisC 17 101 11 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

filename=$prefix'_'big_c_negStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisW 654 12 -13 -axisC 101 17 -11 > tmp 
python check_slice.py slice_out.txt
rm slice_out.txt


W=164
H=101
C=15

filename=$prefix'_'small_c_posStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisW 23 64 2 -axisC -10 101 3 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

filename=$prefix'_'small_c_negStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisW -23 45 -2 -axisC 301 -123 -5 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

mv $prefix* $folder
