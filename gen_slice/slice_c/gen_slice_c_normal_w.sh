elementSize=2

W=64
H=101
C=113

cd code/
make
cp GenMem* ../
cd ../
folder=./testcases/slice/slice_c/normal_w/
cmd=slice
prefix=slice_c_normal_w

filename=$prefix'_'big_c_posStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisC 22 88 2 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

filename=$prefix'_'big_c_negStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisC 88 -43 -13 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt


W=64
H=101
C=13

filename=$prefix'_'small_c_posStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisC 2 8 3 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

filename=$prefix'_'small_c_negStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisC 8 -43 -1 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

mv $prefix* $folder
