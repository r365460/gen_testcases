elementSize=2

W=163
H=101
C=113

cd code/
make
cp GenMem* ../
cd ../
folder=./testcases/slice/slice_h/big_w/
cmd=slice
prefix=slice_h_big_w

filename=$prefix'_'big_c_posStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisH 22 113 2 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

filename=$prefix'_'big_c_negStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisH -1 0 -13 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt


W=164
H=101
C=3

filename=$prefix'_'small_c_posStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisH 0 12 1 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

filename=$prefix'_'small_c_negStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisH 12 1 -1 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

mv $prefix* $folder
