elementSize=2

W=23
H=101
C=113

cd code/
make
cp GenMem* ../
cd ../
folder=./testcases/slice/slice_all/small_w/
cmd=slice
prefix=slice_all_small_w

filename=$prefix'_'big_c_posStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisW 12 24 4 -axisH 30 32 2 -axisC 17 101 11 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

filename=$prefix'_'big_c_negStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisW 24 12 -4 -axisH 32 0 -2 -axisC 101 17 -13 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt



W=23
H=101
C=13

filename=$prefix'_'small_c_posStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisW 7 21 7 -axisH 11 100 13 -axisC 0 101 11 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

filename=$prefix'_'small_c_negStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisW 21 7 -7 -axisH 100 11 -12 -axisC 301 -123 -11 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

mv $prefix* $folder
