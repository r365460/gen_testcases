elementSize=2

W=64
H=101
C=113

cd code/
make
cp GenMem* ../
cd ../
folder=./testcases/slice/slice_both_c_h/normal_w/
cmd=slice
prefix=slice_both_c_h_normal_w

filename=$prefix'_'big_c_posStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisH 0 64 2 -axisC 17 101 11 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

filename=$prefix'_'big_c_negStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisH 654 50 -2 -axisC 101 17 -2 > tmp 
python check_slice.py slice_out.txt
rm slice_out.txt


W=64
H=101
C=13

filename=$prefix'_'small_c_posStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisH 11 23 2 -axisC -10 101 12 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

filename=$prefix'_'small_c_negStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisH 45 -23 -2 -axisC 301 -123 -1 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

mv $prefix* $folder
