elementSize=2

W=163
H=101
C=113

cd code/
make
cp GenMem* ../
cd ../
folder=./testcases/slice/slice_both_c_h/big_w/
cmd=slice
prefix=slice_both_c_h_big_w

filename=$prefix'_'big_c_posStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisH 0 101 2 -axisC 17 101 3 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

filename=$prefix'_'big_c_negStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisH 101 0 -1 -axisC 101 17 -13 > tmp 
python check_slice.py slice_out.txt
rm slice_out.txt


W=164
H=101
C=15

filename=$prefix'_'small_c_posStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisH 0 101 1 -axisC -10 101 3 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

filename=$prefix'_'small_c_negStep
echo $filename' '$W' x '$H' x '$C
./GenMem $W $H $C $elementSize $filename $cmd -perm -axisH 101 0 -3 -axisC 301 -123 -5 > tmp
python check_slice.py slice_out.txt
rm slice_out.txt

mv $prefix* $folder
