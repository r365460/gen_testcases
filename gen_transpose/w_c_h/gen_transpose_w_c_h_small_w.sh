elementSize=2


cd code/
make
cp GenMem* ../
cd ../
folder=./testcases/transpose/w_c_h/small_w
cmd=transpose
perm=" 0 2 1"

W=24
C=12
H=23
filename=transpose_w_c_h_small_w_small_c
./GenMem $W $H $C $elementSize $filename $cmd -perm $perm > tmp
python check_transpose.py transpose_out.txt

mv $filename* $folder


W=24
C=102
H=23
filename=transpose_w_c_h_small_w_big_c
./GenMem $W $H $C $elementSize $filename $cmd -perm $perm > tmp
python check_transpose.py transpose_out.txt

mv $filename* $folder

