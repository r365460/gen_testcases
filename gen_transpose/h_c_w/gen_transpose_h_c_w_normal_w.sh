elementSize=2


cd code/
make
cp GenMem* ../
cd ../
folder=./testcases/transpose/h_c_w/normal_w
cmd=transpose
perm=" 1 2 0"

W=64
C=12
H=23
filename=transpose_h_c_w_normal_w_small_c
./GenMem $W $H $C $elementSize $filename $cmd -perm $perm > tmp
python check_transpose.py transpose_out.txt

mv $filename* $folder



W=64
C=102
H=23
filename=transpose_h_c_w_normal_w_big_c
./GenMem $W $H $C $elementSize $filename $cmd -perm $perm > tmp
python check_transpose.py transpose_out.txt

mv $filename* $folder

