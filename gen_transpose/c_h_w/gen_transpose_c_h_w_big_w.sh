elementSize=2


cd code/
make
cp GenMem* ../
cd ../
folder=./testcases/transpose/c_h_w/big_w
cmd=transpose
perm=" 2 1 0"

W=517
C=12
H=23
filename=transpose_c_h_w_big_w_small_c
./GenMem $W $H $C $elementSize $filename $cmd -perm $perm > tmp
python check_transpose.py transpose_out.txt

mv $filename* $folder


W=517
C=102
H=23
filename=transpose_c_h_w_big_w_big_c
./GenMem $W $H $C $elementSize $filename $cmd -perm $perm > tmp
python check_transpose.py transpose_out.txt

mv $filename* $folder

