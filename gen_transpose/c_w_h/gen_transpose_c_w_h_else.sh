elementSize=2


cd code/
make
cp GenMem* ../
cd ../
folder=./testcases/transpose/c_w_h/mix
cmd=transpose
perm=" 2 0 1"

W=(1 1 3 1 1  17 23 107 13   313  160)
H=(1 1 1 3 17 1  23 113 117  2    20 )
C=(1 3 1 1 17 17 11 119 1117 51   160)

for ((i=0;i<${#W[@]};i++)); do
	w=${W[i]}
	h=${H[i]}
	c=${C[i]}

	filename=transpose_c_w_h_mix_$w'x'$h'x'$c
	echo $filename
	./GenMem $w $h $c $elementSize $filename $cmd -perm $perm > tmp
	python check_transpose.py transpose_out.txt

	mv $filename* $folder

	echo ''
done
